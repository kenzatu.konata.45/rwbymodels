package be.bluexin.rwbym.proxy;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.ICustomItem;
import be.bluexin.rwbym.weaponry.RWBYAmmoEntity;
import be.bluexin.rwbym.weaponry.RWBYAmmoRender;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class ClientProxy extends CommonProxy {

    public void preInit() {
        super.preInit();
        RenderingRegistry.registerEntityRenderingHandler(RWBYAmmoEntity.class, RWBYAmmoRender::new);
//        MinecraftForge.EVENT_BUS.register(RWBYKeybinding.INSTANCE);
//        RWBYKeybinding.INSTANCE.registerKeyBinds(); // TODO: use RWBYKeybind enum too
    }

    public void init() {
        super.init();
        if (RWBYModels.items != null) RWBYModels.items.forEach(ICustomItem::registerModel);
    }
}
