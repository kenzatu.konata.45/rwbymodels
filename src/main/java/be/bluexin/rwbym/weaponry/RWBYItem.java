package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.ItemDTO;
import be.bluexin.rwbym.weaponry.dto.RecipeDTO;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
public class RWBYItem extends Item implements ICustomItem {

    private final RecipeDTO recipe;

    public RWBYItem(ItemDTO from) {
        this.setRegistryName(new ResourceLocation(RWBYModels.MODID, from.getName()));
        this.setUnlocalizedName(this.getRegistryName().toString());
        this.recipe = from.getRecipe();
        this.setCreativeTab(RWBYModels.tab_rwbyitems);
    }

    @Override
    public void registerRecipe() {
        if (this.recipe != null) this.recipe.register(this);
    }

    @Override
    public String toString() {
        return "RWBYItem{" + this.getRegistryName() + "}";
    }
}
