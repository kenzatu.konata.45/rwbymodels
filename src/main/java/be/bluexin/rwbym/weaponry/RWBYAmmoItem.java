package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.AmmoCapDTO;
import be.bluexin.rwbym.weaponry.dto.AmmoDTO;
import be.bluexin.rwbym.weaponry.dto.RecipeDTO;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
public class RWBYAmmoItem extends Item implements ICustomItem {

    private final RecipeDTO recipe;
    private final AmmoCapDTO capabilities;

    public RWBYAmmoItem(AmmoDTO from) {
        this.setCreativeTab(RWBYModels.tab_rwbyweapons);
        this.setRegistryName(new ResourceLocation(RWBYModels.MODID, from.getName()));
        this.setUnlocalizedName(this.getRegistryName().toString());
        this.recipe = from.getRecipe();
        this.capabilities = from.getEntityCapabilities();
    }

    public EntityArrow createArrow(World worldIn, ItemStack stack, EntityLivingBase shooter) {
        return new RWBYAmmoEntity(worldIn, shooter, this.capabilities, this);
    }

    public boolean isInfinite(@Nullable ItemStack stack, @Nullable ItemStack bow, @Nullable EntityPlayer player) {
        return capabilities.isInfinite() || (bow != null && EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, bow) > 0);
    }

    @Override
    public void registerRecipe() {
        if (this.recipe != null) this.recipe.register(this);
    }

    @Override
    public String toString() {
        return "RWBYAmmoItem{" + this.getRegistryName() + "}";
    }
}
