package be.bluexin.rwbym.weaponry.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class CollectionDTO {
    private final List<SwordDTO> swords = new ArrayList<>();
    private final List<BowDTO> bows = new ArrayList<>();
    private final List<ItemDTO> items = new ArrayList<>();
    private final List<AmmoDTO> ammo = new ArrayList<>();

    public List<SwordDTO> getSwords() {
        return swords;
    }

    public List<BowDTO> getBows() {
        return bows;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public List<AmmoDTO> getAmmo() {
        return ammo;
    }
}
