package be.bluexin.rwbym.weaponry.dto;

import be.bluexin.rwbym.RWBYModels;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * Part of rwbymodels
 *
 * @author Bluexin
 */
public class RecipeDTO {
    private final RecipeType type;
    private final int resultAmount;
    private final String[] recipeLines;
    private final Map<Character, String> recipeMap;

    public RecipeDTO(RecipeType type, int resultAmount, String[] recipeLines, Map<Character, String> recipeMap) {
        this.type = type;
        this.resultAmount = resultAmount;
        this.recipeLines = recipeLines;
        this.recipeMap = recipeMap;
    }

    public void register(Item result) {
        LogManager.getLogger(RWBYModels.MODID).debug("Registering " + this);
        this.type.register(result, this);
    }

    @Override
    public String toString() {
        return "RecipeDTO{" +
                "type=" + type +
                ", resultAmount=" + resultAmount +
                ", recipeLines=" + Arrays.toString(recipeLines) +
                ", recipeMap=" + recipeMap +
                '}';
    }

    @SuppressWarnings("unused")
    private enum RecipeType {
        SHAPED((result, info) -> {
            ItemStack res = new ItemStack(result, info.resultAmount);
            Object[] args = new Object[info.recipeLines.length + info.recipeMap.size() * 2];

            System.arraycopy(info.recipeLines, 0, args, 0, info.recipeLines.length);
            Character[] chars = info.recipeMap.keySet().toArray(new Character[0]);
            String[] values = info.recipeMap.values().toArray(new String[0]);
            Item it;
            boolean ok = true;
            for (int i = 0; i < chars.length; i++) {
                args[i * 2 + info.recipeLines.length] = chars[i];
                args[i * 2 + 1 + info.recipeLines.length] = new ItemStack(it = Item.getByNameOrId(values[i]));
                //noinspection ConstantConditions -> yes it can be null u_u
                if (it == null) {
                    LogManager.getLogger(RWBYModels.MODID).warn("Found an invalid id: " + values[i]);
                    ok = false;
                }
            }

            //noinspection ConstantConditions -> it can be false
            if (ok) GameRegistry.addShapedRecipe(res, args);
        }),
        SHAPELESS((result, info) -> {
            ItemStack res = new ItemStack(result, info.resultAmount);
            ItemStack[] recipe = new ItemStack[info.recipeLines.length];

            for (int i = 0; i < info.recipeLines.length; i++)
                recipe[i] = new ItemStack(Item.getByNameOrId(info.recipeLines[i]));

            GameRegistry.addShapelessRecipe(res, (Object[]) recipe);
        }),
        FURNACE((result, info) -> GameRegistry.addSmelting(info.recipeLines.length > 1 ? new ItemStack(Item.getByNameOrId(info.recipeLines[0]), Integer.parseInt(info.recipeLines[1])) : new ItemStack(Item.getByNameOrId(info.recipeLines[0])), new ItemStack(result, info.resultAmount), info.recipeLines.length > 2 ? Float.parseFloat(info.recipeLines[2]) : Float.parseFloat(info.recipeLines[1])));

        private final BiConsumer<Item, RecipeDTO> register;

        RecipeType(BiConsumer<Item, RecipeDTO> register) {
            this.register = register;
        }

        public void register(Item result, RecipeDTO info) {
            this.register.accept(result, info);
        }
    }
}
