package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym by Bluexin.
 * <p>
 * Refers only to non-equipment type items (crafting mats)
 *
 * @author Bluexin
 */
public class ItemDTO {
    private final String name;
    private final RecipeDTO recipe;

    public ItemDTO(String name, RecipeDTO recipe) {
        this.name = name;
        this.recipe = recipe;
    }

    public String getName() {
        return name;
    }

    public RecipeDTO getRecipe() {
        return recipe;
    }
}
