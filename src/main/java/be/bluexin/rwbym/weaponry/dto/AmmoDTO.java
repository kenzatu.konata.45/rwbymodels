package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
public class AmmoDTO {
    private final String name;
    private final RecipeDTO recipe;
    private final AmmoCapDTO entityCapabilities;

    public AmmoDTO(String name, RecipeDTO recipe, AmmoCapDTO entityCapabilities) {
        this.name = name;
        this.recipe = recipe;
        this.entityCapabilities = entityCapabilities;
    }

    public String getName() {
        return name;
    }

    public RecipeDTO getRecipe() {
        return recipe;
    }

    public AmmoCapDTO getEntityCapabilities() {
        return entityCapabilities;
    }
}
